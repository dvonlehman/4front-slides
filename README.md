# 4front-slides

Slideshow built with [reveal.js](http://lab.hakim.se/reveal-js/) about modern multi-tenant web hosting and the [4front](http://4front.io) framework.

View the slideshow at [http://4front-slides.aerobatic.io](http://4front-slides.aerobatic.io).
